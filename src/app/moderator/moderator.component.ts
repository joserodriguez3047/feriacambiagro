import { Component, OnInit } from '@angular/core';
import  {  faUser, faCartShopping, faList, faStore  }  from  '@fortawesome/free-solid-svg-icons' ;
import { EquipoService, Equipo } from '../services/equipo.service';

@Component({
  selector: 'app-moderator',
  templateUrl: './moderator.component.html',
  styleUrls: ['./moderator.component.scss']
})
export class ModeratorComponent implements OnInit {
  faUser  =  faUser ;
  faCartShopping = faCartShopping;
  faList = faList;
  faStore = faStore;
  ListarEquipo: Equipo[];
  constructor(private EquipoService:EquipoService) { }

  ngOnInit(): void {
    this.listarEquipo();
  }

  listarEquipo(){
    this.EquipoService.getEquipos().subscribe(
      res=>{
        console.log(res);
        this.ListarEquipo=<any>res;
      },
      err=> console.log(err)
    );
  }

  editproducto(entity_id:any, stat:number){
    const equipo  = {status: stat};
    this.EquipoService.editEquipo(entity_id, equipo).subscribe(
      res=>{
        console.log('producto actualizado');
      });
  }

}
