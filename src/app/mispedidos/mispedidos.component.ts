import { Component, OnInit } from '@angular/core';
import  {  faUser, faCartShopping, faList, faStore  }  from  '@fortawesome/free-solid-svg-icons' ;
import { EquipoService, Orden } from '../services/equipo.service';
import { io } from 'socket.io-client';
import {Router} from "@angular/router";

@Component({
  selector: 'app-mispedidos',
  templateUrl: './mispedidos.component.html',
  styleUrls: ['./mispedidos.component.scss']
})
export class MispedidosComponent implements OnInit {
  public userid:any = null; 
  faUser  =  faUser ;
  faCartShopping = faCartShopping;
  faList = faList;
  faStore = faStore;
  ListarOrdenes: Orden[];
  public inputValue = 0;
  public entity = 0;
  public qty = 0;
  public isEdit = false;
  io = io("https://www.eventoscambiagro.com/", {
    withCredentials: true,
    autoConnect: true
  });

  constructor(private EquipoService:EquipoService, private router: Router) {
    this.entity = 0;
    this.qty = 0;
    this.userid = localStorage.getItem('currentUser');
    if(!this.userid){
      this.router.navigate(['/']);
    }
    this.io.on("showorder", ()=>{
      this.listarOrdenes();
    });
   }

  ngOnInit(): void {
    this.userid = localStorage.getItem('currentUser');
    this.listarOrdenes();
  }

  cambiarEntity(entity:any, qty:any){
    this.isEdit = true;
    this.entity = entity;
    this.qty = qty;
  }

  listarOrdenes(){
    this.EquipoService.getOrdenes(this.userid).subscribe((data:any) =>{
      this.ListarOrdenes=data;
    });
  }

  cerrarsesion(){
    localStorage.removeItem('currentUser');
  }

  editCompra(entity:any, data:any){
    console.log(entity);
    this.isEdit=false;
    const equipo  = {qty: data.qty};
    this.EquipoService.editOrden(entity, equipo).subscribe((data:any)=>{
      this.ListarOrdenes=data;
    });
    window.location.reload();
  }

  cerrardialog(){
    this.isEdit = false;
  }

  onKey(event:any) { this.inputValue = event.target.value;}

  eliminar(id:any){
    this.EquipoService.deleteOrden(id).subscribe((data:any)=>{
      this.ListarOrdenes=data;
    });
    window.location.reload();
  }

}
