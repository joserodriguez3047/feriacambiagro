import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { io } from 'socket.io-client';
import { AppComponent } from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class EquipoService {
  public userData: number;
  //public dominio = 'http://localhost:3000/';
  public dominio = 'https://www.eventoscambiagro.com/';
  url = this.dominio+'api';
  
  io = io(this.dominio, {
    withCredentials: true,
    autoConnect: true
  });
  constructor(private http:HttpClient ) { 

  }

  getReportePedidos(){
    return this.http.get(this.url+'/pedidos');
  }

  getReporteCliente(){
    return this.http.get(this.url+'/pedidocliente');
  }

  getEquipos(){
    return this.http.get(this.url);
  }

  getOrdenes(id:number){
    return this.http.get(this.url+'/order/'+id);
  }

  compra(orden:any){
    return this.http.post(this.url+'/order', orden);
  }

  login(usr:any){
    return this.http.post(this.url+'/login', usr);
  }

  editEquipo(id:number, equipo:any): Observable <any> {
    this.io.emit("editProduct");
    return this.http.put(this.url+'/'+id, equipo);
  }

  editOrden(id:number, qty:any): Observable <any> {
    return this.http.put(this.url+'/order/'+id, qty);
  }

  deleteOrden(id:any) {
    this.io.emit("deleteProduct");
    return this.http.delete(this.url+'/order/'+id);
  }

  busquedaPorNombre(name:string){
    return this.http.get(this.url+'/busqueda/'+name);
  }

  busquedaPorCategoria(categoria:number){
    return this.http.get(this.url+'/buscarcategoria/'+categoria);
  }
}

export interface Equipo{
  entity_id?: number,
  detail?: string,
  name?: string,
  stock?: number,
  price?: number,
  status?: number,
  image?: string,
  baseprice?: number,
  originalprice: number
}

export interface Orden{
  entity_id?: number,
  created_at: Date,
  update_at: Date,
  total: number,
  customer_group_code: number,
  vendedor: number,
  product_id: number,
  price: number,
  qty:number,
  name:string,
  entity_product:number
}

export interface ReportePedidos{
  name?: string,
  qty: number, 
  total: number
}

export interface ReporteCliente{
  cliente: string,
  ppc: number,
  pnc: number,
  foliares: number,
  semillas: number,
  estrategicos: number,
  commodities: number
}