import { Component, OnInit, ViewChild } from '@angular/core';
import  {  faUser, faCartShopping, faList, faStore, faLeaf, faSearch  }  from  '@fortawesome/free-solid-svg-icons' ;
import { NgSelectModule } from '@ng-select/ng-select';
import { Subscription } from 'rxjs';
import { EquipoService, Equipo } from '../services/equipo.service';
import { ActivatedRoute } from '@angular/router';
import * as sha256 from 'crypto-js/sha256';
import { io } from 'socket.io-client';
import {Router} from "@angular/router";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  public userid:any = null; 
  msg ="";
  faUser  =  faUser ;
  faCartShopping = faCartShopping;
  faList = faList;
  faStore = faStore;
  search =faSearch;
  ListarEquipo: Equipo[];
  suscription:Subscription;
  public alert = false;
  public busqueda = false;
  io = io("https://www.eventoscambiagro.com/", {
    withCredentials: true,
    autoConnect: true
  });

  constructor(private EquipoService:EquipoService, private router: Router) {
    this.busqueda = false;
    this.userid = localStorage.getItem('currentUser');
    if(!this.userid){
      this.router.navigate(['/']);
    }
    this.io.on("show", ()=>{
      this.listarEquipo();
    });

  }

  ngOnInit(): void {
    this.listarEquipo();
  }

  cerrarsesion(){
    localStorage.removeItem('currentUser');
  }

  parserjson(data:any){
    let dataparce = JSON.parse(data);
    let text = '<table style="border: 1px; font-size: 13px;" width=100%>'
    for (let x in dataparce) {
      text += "<tr><td width=75%>" + dataparce[x].name + "</td>";
      text += "<td width=25%>" + dataparce[x].qty + "</td></tr>";
    }
    text += "</table>"
    return text;
  }

  busquedaNombre(data:any){
    this.busqueda = true;
    if(data.q){
    const busqueda = data.q;
    this.EquipoService.busquedaPorNombre(busqueda).subscribe((data:any ) =>{
      this.ListarEquipo=data;
    });
    }
  }

  buscarcategoria(data:any){
    this.busqueda = true;
      this.EquipoService.busquedaPorCategoria(data).subscribe((data:any ) =>{
        this.ListarEquipo=data;
      });
  }

  compra(name:any, total:any, vendedor:number, customer_group_code: number, product_id:any, price:any, data:any){
    const orden = {total:(price*data.qty), vendedor:vendedor, customer_group_code:customer_group_code, product_id:product_id, price:price, qty:data.qty}
    this.msg = name;
    this.alert = true;
    this.EquipoService.compra(orden).subscribe(
      res=>{
        console.log('producto comprado');
      });
  }

  ocultar(){
    setTimeout( () => { this.alert = false }, 5000 );
  }

  listarEquipo(){
    this.busqueda = false;
    this.EquipoService.getEquipos().subscribe((data:any )=>{
      this.ListarEquipo=data;
    });
  }

}
