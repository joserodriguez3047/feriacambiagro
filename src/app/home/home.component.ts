import { Component, OnInit } from '@angular/core';
import { EquipoService } from '../services/equipo.service';
import {Router} from "@angular/router";
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private EquipoService:EquipoService, private router: Router) { }

  ngOnInit(): void {
  }

  login(data:any){
    const usr = {username:data.username, password:data.password}
    this.EquipoService.login(usr).subscribe((data:any) =>{
        localStorage.setItem('currentUser', data);
        if(data){
          this.router.navigate(['/client']);
        }else{
          window.location.reload();
        }
    });
  }

}
