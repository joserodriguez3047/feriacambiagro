import { Component, OnInit } from '@angular/core';
import  {  faUser, faCartShopping, faList, faStore  }  from  '@fortawesome/free-solid-svg-icons' ;
import { ReportePedidos, EquipoService, ReporteCliente } from '../services/equipo.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  ReporteVentas: ReportePedidos[];
  ReporteCliente: ReporteCliente[];
  faUser  =  faUser ;
  faCartShopping = faCartShopping;
  faList = faList;
  faStore = faStore;
  view: [number, number] = [1200, 300];
  showXAxis = true;
  showYAxis = true;
  gradient2 = false;
  showLegend2 = true;
  showXAxisLabel = true;
  xAxisLabel = 'Cliente';
  showYAxisLabel = true;
  yAxisLabel = 'Quetzales';
  xAxisLabel2 = 'Producto';

  single = [
    {
      "name": "APENSA EL SEMILLERO",
      "value": 89400
    },
    {
      "name": "SURTIVEL",
      "value": 50000
    },
    {
      "name": "SEÑOR AGRO S.A.",
      "value": 72000
    },
      {
      "name": "AGROQUICHE, S.A.",
      "value": 62000
    },
    {
      "name": "APENSA EL SEMILLERO1",
      "value": 89400
    },
    {
      "name": "SURTIVEL1",
      "value": 50000
    },
    {
      "name": "SEÑOR AGRO S.A.1",
      "value": 72000
    },
      {
      "name": "AGROQUICHE, S.A.1",
      "value": 62000
    }
  ];

  product = [
    {
      "name": "PNC",
      "value": 89400
    },
    {
      "name": "PPC",
      "value": 50000
    },
    {
      "name": "Semillas",
      "value": 72000
    }
  ];

  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;

  constructor(private EquipoService:EquipoService) {
    
  }

  ngOnInit(): void {
    this.EquipoService.getReportePedidos().subscribe((data:any ) =>{
      this.ReporteVentas=data;
    });

    this.EquipoService.getReporteCliente().subscribe((data:any ) =>{
      this.ReporteCliente=data;
    });
  }

   onSelect(data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

}

