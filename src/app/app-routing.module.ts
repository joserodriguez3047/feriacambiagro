import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartComponent } from './chart/chart.component';
import { AppComponent } from './app.component';
import { ClientComponent } from './client/client.component';
import { HomeComponent } from './home/home.component';
import { ModeratorComponent } from './moderator/moderator.component';
import { RestorepasswordComponent } from './restorepassword/restorepassword.component';
import { SignupComponent } from './signup/signup.component';
import { MispedidosComponent } from './mispedidos/mispedidos.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'moderator', component: ModeratorComponent},
  {path: 'restore', component: RestorepasswordComponent},
  {path: 'client', component: ClientComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'client/:id', component: ClientComponent},
  {path: 'moderator/chart', component: ChartComponent},
  {path: 'mispedidos', component: MispedidosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
